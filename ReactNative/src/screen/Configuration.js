import React from 'react';
import { Button, View, StyleSheet } from 'react-native';

const ConfigurationScreen = ( {navigation, route} ) => {
    return (
      <View style={styles.mainView}>
      <View style={styles.container}>
        <View style={styles.container}>

          <Button
            title="Home"
            onPress={() => {navigation.navigate("Home")}}/>

          <Button
            title="Ver Dados"
            onPress={() => {navigation.navigate("Exibição de Dados")}}/>
        </View>
        </View>
      </View>
    )
};

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    paddingTop: 370,
    paddingBottom: 70,
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  container: {
    flex:1,
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "space-around"
  }
});

export default ConfigurationScreen;