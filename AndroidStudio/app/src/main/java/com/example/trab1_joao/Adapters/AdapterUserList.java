package com.example.trab1_joao.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.trab1_joao.Model.User;
import com.example.trab1_joao.R;

import java.util.List;

public class AdapterUserList extends BaseAdapter {
    private Context context;
    private List<User> UserList;

    public AdapterUserList(Context context, List<User> UserList){
        this.context = context;
        this.UserList = UserList;
    }

    @Override
    public int getCount() {
        return UserList.size();
    }

    @Override
    public Object getItem(int position) {
        return UserList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public int getUserId(int position) {
        return UserList.get(position).Id;
    }

    public void removeUser(int position) {
        UserList.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = View.inflate(this.context, R.layout.layout_user_list, null);

        TextView tvNameUser = (TextView) v.findViewById(R.id.tvNameUser);
        TextView tvEmailUser = (TextView) v.findViewById(R.id.tvEmailUser);
        tvNameUser.setText(UserList.get(position).Name);
        tvEmailUser.setText(UserList.get(position).Email);

        return v;
    }
}
