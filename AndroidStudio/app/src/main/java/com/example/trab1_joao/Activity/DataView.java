package com.example.trab1_joao.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.trab1_joao.Database.Trab1OpenHelper;
import com.example.trab1_joao.Database.UserDAO;
import com.example.trab1_joao.Model.User;
import com.example.trab1_joao.R;

import java.io.File;

public class DataView extends AppCompatActivity {

    Button btnRemove;
    Button btnEdit;
    int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_view);
        Context context = getApplicationContext();
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            userId = extras.getInt("UserId");
        }
        if(userId > 0){
            Trab1OpenHelper conexao = Trab1OpenHelper.getInstancia(context);
            UserDAO db = new UserDAO(conexao);
            User item = db.getUser(userId);
            EditText Name = (EditText) findViewById(R.id.editTextDataViewName);
            EditText Phone = (EditText) findViewById(R.id.editTextDataViewPhoneNumber);
            EditText Email = (EditText) findViewById(R.id.editTextDataViewEmail);
            CheckBox cb1 = (CheckBox) findViewById(R.id.cbDataView1);
            CheckBox cb2 = (CheckBox) findViewById(R.id.cbDataView2);
            ImageView img = (ImageView) findViewById(R.id.imageView);

            Name.setText(item.Name);
            Phone.setText(item.Phone);
            Email.setText(item.Email);
            if(item.Gender == 0 || item.Gender == 1){
                cb1.setChecked(true);
            }
            else if(item.Gender == 2){
                cb2.setChecked(true);
            }
            if(item.img != null && item.img != ""){
                File imgFile = new  File(item.img);
                if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    img.setVisibility(View.VISIBLE);
                    img.setImageBitmap(myBitmap);
                }
            }
        }
        else{
            Toast toast = Toast.makeText(context, "Falha ao carregar!", Toast.LENGTH_LONG);
            toast.show();
        }

        btnRemove = (Button) findViewById(R.id.btnDataViewRemove);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Trab1OpenHelper conexao = Trab1OpenHelper.getInstancia(context);
                UserDAO db = new UserDAO(conexao);
                boolean response = db.removeUser(userId);
                if(response){
                    Intent intent = new Intent(context, UserListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                else{
                    Toast toast = Toast.makeText(context, "Falha ao remover!", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        btnEdit = (Button) findViewById(R.id.btnDataViewEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Context context = getApplicationContext();
                Intent intent = new Intent(context, EditUser.class);
                intent.putExtra("UserId", userId);
                startActivity(intent);
            }
        });
    }
}