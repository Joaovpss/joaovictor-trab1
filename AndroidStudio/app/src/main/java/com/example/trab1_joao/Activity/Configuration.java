package com.example.trab1_joao.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.trab1_joao.R;

public class Configuration extends AppCompatActivity {

    Button btnMainActivity;
    Button btnDataView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        btnMainActivity = (Button) findViewById(R.id.btnConfigurationRegister);
        btnMainActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Context context = getApplicationContext();
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            }
        });

        btnDataView = (Button) findViewById(R.id.btnConfigurationView);
        btnDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Context context = getApplicationContext();
                Intent intent = new Intent(context, DataView.class);
                startActivity(intent);
            }
        });
    }
}