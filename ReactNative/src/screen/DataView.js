import React, { useState, useEffect } from 'react';
import { Button, View, Text, StyleSheet, Switch, Image } from 'react-native';


const DataViewScreen = ({navigation, route} ) => {
  const [Name, setName] = useState('');
  const [Phone, setPhone] = useState('');
  const [Email, setEmail] = useState('');
  const [GenderM, setGenderM] = useState(false);
  const [GenderF, setGenderF] = useState(false);

  useEffect(() => {
  if(route.params != null){
    setName(route.params.user.name);
    setPhone(route.params.user.phone);
    setEmail(route.params.user.email);
    setGenderM(route.params.user.genderM);
    setGenderF(route.params.user.genderF);
  }
  }, []);

    return (
      <View style={styles.mainView}>

        <View style={styles.container}>
                <Text>Nome: </Text>
                <Text>{Name}</Text>
        </View>

        <View style={styles.container}>
                <Text>Telefone: </Text>
                <Text>{Phone}</Text>
        </View>

        <View style={styles.container}>
                <Text>Email: </Text>
                <Text>{Email}</Text>
        </View>

        <View style={styles.container}>
              <Text>Masculino: </Text>
                <Switch
                  trackColor={{ false: "#767577", true: "#81b0ff" }}
                  thumbColor={GenderM ? "#f5dd4b" : "#f4f3f4"}
                  ios_backgroundColor="#3e3e3e"
                  value={GenderM}
                  disabled={true}
                />
        </View>

        <View style={styles.container}>
              <Text>Feminino: </Text>
                <Switch
                  trackColor={{ false: "#767577", true: "#81b0ff" }}
                  thumbColor={GenderF ? "#f5dd4b" : "#f4f3f4"}
                  ios_backgroundColor="#3e3e3e"
                  value={GenderF}
                  disabled={true}
                />
        </View>

        <View style={styles.container}>

          <View style={styles.container}>

            <Button
              title="Registrar"
              onPress={() => {navigation.navigate("Home")}}/>

            <Button
              title="Configuração"
              onPress={() => {navigation.navigate("Configuração")}}/>
          </View>
        </View>
      </View>
    )
};

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    paddingTop: 70,
    paddingBottom: 70,
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  container: {
    flex:1,
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "space-around"
  }
});

export default DataViewScreen;