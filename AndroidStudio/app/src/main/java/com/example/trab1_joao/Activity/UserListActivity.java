package com.example.trab1_joao.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.trab1_joao.Adapters.AdapterUserList;
import com.example.trab1_joao.Database.Trab1OpenHelper;
import com.example.trab1_joao.Database.UserDAO;
import com.example.trab1_joao.Model.User;
import com.example.trab1_joao.R;

import java.util.ArrayList;
import java.util.List;

public class UserListActivity extends AppCompatActivity {

    private ListView lvUser;
    private List<User> UserList;
    private AdapterUserList adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        Context context = getApplicationContext();
        Trab1OpenHelper conexao = Trab1OpenHelper.getInstancia(context);
        UserDAO db = new UserDAO(conexao);
        UserList = db.getUserList();
        lvUser = (ListView) findViewById(R.id.lvUsers);
        if(UserList != null){
            adapter = new AdapterUserList(context, UserList);
        }
        else{
            Toast toast = Toast.makeText(context, "Falha ao carregar lista!", Toast.LENGTH_LONG);
            toast.show();
        }
        lvUser.setAdapter(adapter);

        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context, DataView.class);
                intent.putExtra("UserId", adapter.getUserId(position));
                startActivity(intent);
            }
        });
    }
}