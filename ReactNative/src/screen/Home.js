import React, {useState} from 'react';
import { Button, View, Text, StyleSheet, TextInput, Switch } from 'react-native';

const HomeScreen = ( {navigation, route} ) => {
    const [Name, setName] = useState('');
    const [Phone, setPhone] = useState('');
    const [Email, setEmail] = useState('');
    const [GenderM, setGenderM] = useState(true);
    const [GenderF, setGenderF] = useState(false);

    function changeGenderM(value){
        setGenderM(value);
        setGenderF(!value);
    }

    function changeGenderF(value){
        setGenderF(value);
        setGenderM(!value);
    }

    return (
      <View style={styles.mainView}>

        <View style={styles.container}>
                <Text>Nome: </Text>
                <TextInput
                style={{ height: 35, width: 200, borderColor: 'gray', borderWidth: 1}}
                placeholder=" Digite seu Nome..."
                onChangeText = {texto => setName(texto)}
                value={Name}
              />
        </View>
              
        <View style={styles.container}>
                <Text>Telefone: </Text>
                <TextInput
                style={{ height: 35, width: 200, borderColor: 'gray', borderWidth: 1}}
                placeholder=" Digite seu Telefone..."
                onChangeText = {texto => setPhone(texto)}
                value={Phone}
              />
        </View>

        <View style={styles.container}>
              <Text>Email: </Text>
              <TextInput
                style={{ height: 35, width: 200, borderColor: 'gray', borderWidth: 1}}
                placeholder=" Digite seu Email..."
                onChangeText = {texto => setEmail(texto)}
                value={Email}
                />
        </View>

        <View style={styles.container}>
              <Text>Masculino: </Text>
                <Switch
                  trackColor={{ false: "#767577", true: "#81b0ff" }}
                  thumbColor={GenderM ? "#f5dd4b" : "#f4f3f4"}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange= {value => changeGenderM(value)}
                  value={GenderM}
                />
        </View>

        <View style={styles.container}>
              <Text>Feminino: </Text>
                <Switch
                  trackColor={{ false: "#767577", true: "#81b0ff" }}
                  thumbColor={GenderF ? "#f5dd4b" : "#f4f3f4"}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange= {value => changeGenderF(value)}
                  value={GenderF}
                />
        </View>

        <View style={styles.container}>

          <View style={styles.container}>
            <Button
              title="Ver dados"
              onPress={() => {navigation.navigate("Exibição de Dados", {
                user: {
                  name: Name,
                  phone: Phone,
                  email: Email,
                  genderM: GenderM,
                  genderF: GenderF
                }})}}/>

            <Button
              title="Configuração"
              onPress={() => {navigation.navigate("Configuração")}}/>
          </View>
        </View>
      </View>
    )
};

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    paddingTop: 30,
    paddingBottom: 50,
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  container: {
    flex:1,
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "space-around"
  }
});

export default HomeScreen;