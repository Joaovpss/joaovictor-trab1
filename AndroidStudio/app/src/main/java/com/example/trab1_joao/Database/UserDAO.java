package com.example.trab1_joao.Database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.trab1_joao.Model.User;

import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    private final Trab1OpenHelper conexao;

    public UserDAO(Trab1OpenHelper conexao){
        this.conexao = conexao;
    }

    public long SaveUser(String Name, String Phone, String Email, int Gender, String img){
        SQLiteDatabase db = null;
        try{
            db = conexao.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("name", Name);
            values.put("phone", Phone);
            values.put("email", Email);
            values.put("gender", Gender);
            values.put("image", img);
            long Id = db.insert("user_info", null, values);
            return Id;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(db != null){
                db.close();
            }
        }
        return -1;
    }

    public List<User> getUserList(){
        SQLiteDatabase db = null;
        try{
            List<User> response = new ArrayList<>();
            Cursor cursor;
            db = conexao.getReadableDatabase();
            cursor = db.rawQuery("SELECT * FROM user_info", null);
            if(cursor.moveToFirst()){
                do{
                    User aux = new User();
                    aux.Id = cursor.getInt(0);
                    aux.Name = cursor.getString(1);
                    aux.Email = cursor.getString(3);
                    response.add(aux);
                } while (cursor.moveToNext());
            }
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(db != null){
                db.close();
            }
        }
        return null;
    }

    public User getUser(int Id){
        SQLiteDatabase db = null;
        try{
            User response = new User();
            Cursor cursor;
            db = conexao.getReadableDatabase();
            cursor = db.rawQuery("SELECT * FROM user_info WHERE _id = " + Id, null);
            if(cursor.moveToFirst()){
                response.Id = cursor.getInt(0);
                response.Name = cursor.getString(1);
                response.Phone = cursor.getString(2);
                response.Email = cursor.getString(3);
                response.Gender = cursor.getInt(4);
                response.img = cursor.getString(5);
            }
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(db != null){
                db.close();
            }
        }
        return null;
    }

    public boolean attUser(int Id, String Name, String Phone, String Email, int Gender, String img){
        SQLiteDatabase db = null;
        try{
            db = conexao.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("name", Name);
            values.put("phone", Phone);
            values.put("email", Email);
            values.put("gender", Gender);
            values.put("image", img);
            db.update("user_info", values,  "_id = ?", new String[]{String.valueOf(Id)});
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(db != null){
                db.close();
            }
        }
        return false;
    }

    public boolean removeUser(int Id){
        SQLiteDatabase db = null;
        try{
            db = conexao.getReadableDatabase();
            db.delete("user_info", "_id = ?", new String[]{String.valueOf(Id)});
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(db != null){
                db.close();
            }
        }
        return false;
    }
}
