package com.example.trab1_joao.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trab1_joao.Database.Trab1OpenHelper;
import com.example.trab1_joao.Database.UserDAO;
import com.example.trab1_joao.Model.User;
import com.example.trab1_joao.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EditUser extends AppCompatActivity {
    Button btnEdit;
    int userId;

    ImageButton btnImage;
    String currentPhotoPath;
    Uri uriSaveImage;
    String img = null;

    Boolean HasImage = false;

    private static final int REQUEST_IMAGE_CAPTURE = 98;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        CheckBox cb1 = (CheckBox) findViewById(R.id.cbEditUser);
        CheckBox cb2 = (CheckBox) findViewById(R.id.cbEditUser2);
        Context context = getApplicationContext();
        Trab1OpenHelper conexao = Trab1OpenHelper.getInstancia(context);
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            userId = extras.getInt("UserId");
        }
        if(userId > 0){
            UserDAO db = new UserDAO(conexao);
            User item = db.getUser(userId);
            EditText Name = (EditText) findViewById(R.id.editTextEditUserName);
            EditText Phone = (EditText) findViewById(R.id.editTextEditUserPhoneNumber);
            EditText Email = (EditText) findViewById(R.id.editTextEditUserEmail);

            Name.setText(item.Name);
            Phone.setText(item.Phone);
            Email.setText(item.Email);
            if(item.Gender == 0 || item.Gender == 1){
                cb1.setChecked(true);
                cb2.setChecked(false);
            }
            else if(item.Gender == 2){
                cb1.setChecked(false);
                cb2.setChecked(true);
            }
            if(item.img != null && item.img != ""){
                img = item.img;
                TextView text = (TextView) findViewById(R.id.txtEditUserImageNotFound);
                text.setText("Já possui imagem!");
            }
        }
        else{
            Toast toast = Toast.makeText(context, "Falha ao carregar!", Toast.LENGTH_LONG);
            toast.show();
        }

        btnEdit = (Button) findViewById(R.id.btnEditUserSave);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                EditText Name = (EditText) findViewById(R.id.editTextEditUserName);
                EditText Phone = (EditText) findViewById(R.id.editTextEditUserPhoneNumber);
                EditText Email = (EditText) findViewById(R.id.editTextEditUserEmail);
                if(Name.getText().toString().isEmpty()){
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, "Campo Nome é obrigatório!", Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                if(Phone.getText().toString().isEmpty()){
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, "Campo Telefone é obrigatório!", Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                if(Email.getText().toString().isEmpty()){
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, "Campo Email é obrigatório!", Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                int Gender = 0;
                if(cb1.isChecked())
                    Gender = 1;
                if(cb2.isChecked())
                    Gender = 2;
                if(HasImage)
                    img = currentPhotoPath;
                UserDAO db = new UserDAO(conexao);
                boolean response = db.attUser(userId, Name.getText().toString(), Phone.getText().toString(), Email.getText().toString(), Gender, img);
                if(response){
                    Intent intent = new Intent(context, UserListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                else{
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, "Falha ao alterar!", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        btnImage = (ImageButton) findViewById(R.id.btnEditUserCaptureImage);
        btnImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Context context = getApplicationContext();
                try{
                    uriSaveImage = FileProvider.getUriForFile(context, "com.example.trab1_joao.fileprovider", createImageFile());
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSaveImage);

                    if(intent.resolveActivity(getPackageManager()) != null){
                        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            private File createImageFile() throws IOException {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp + "_";
                File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                File image = File.createTempFile(
                        imageFileName,
                        ".jpg",
                        storageDir
                );

                currentPhotoPath = image.getAbsolutePath();
                return image;
            }
        });

        cb1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                cb1.setChecked(true);
                cb2.setChecked(false);
            }
        });
        cb2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                cb1.setChecked(false);
                cb2.setChecked(true);
            }
        });
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            TextView text = (TextView) findViewById(R.id.txtEditUserImageNotFound);
            text.setText("Imagem anexada!");
            HasImage = true;
        }
    }
}