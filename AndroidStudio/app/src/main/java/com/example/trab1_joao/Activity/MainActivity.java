package com.example.trab1_joao.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trab1_joao.Database.Trab1OpenHelper;
import com.example.trab1_joao.Database.UserDAO;
import com.example.trab1_joao.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    Button btnMail;
    Button btnRegister;
    Button btnUserList;

    CheckBox cb1;
    CheckBox cb2;

    ImageButton btnImage;
    String currentPhotoPath;
    Uri uriSaveImage;

    Boolean HasImage = false;

    private static final int REQUEST_IMAGE_CAPTURE = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cb1 = (CheckBox) findViewById(R.id.cbMainActivity1);
        cb2 = (CheckBox) findViewById(R.id.cbMainActivity2);
        Trab1OpenHelper conexao = Trab1OpenHelper.getInstancia(this);

        btnRegister = (Button) findViewById(R.id.btnMainActivityRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                EditText Name = (EditText) findViewById(R.id.editTextMainActivityName);
                EditText Phone = (EditText) findViewById(R.id.editTextMainActivityPhoneNumber);
                EditText Email = (EditText) findViewById(R.id.editTextMainActivityEmail);
                if(Name.getText().toString().isEmpty()){
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, "Campo Nome é obrigatório!", Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                if(Phone.getText().toString().isEmpty()){
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, "Campo Telefone é obrigatório!", Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                if(Email.getText().toString().isEmpty()){
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, "Campo Email é obrigatório!", Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                int Gender = 0;
                if(cb1.isChecked())
                    Gender = 1;
                if(cb2.isChecked())
                    Gender = 2;
                String img = null;
                if(HasImage)
                    img = currentPhotoPath;
                UserDAO db = new UserDAO(conexao);
                long response = db.SaveUser(Name.getText().toString(), Phone.getText().toString(), Email.getText().toString(), Gender, img);
                if(response != -1){
                    TextView text = (TextView) findViewById(R.id.txtImageNotFound);
                    text.setText("Sem imagem anexada!");
                    HasImage = false;
                    currentPhotoPath = null;
                    Name.setText(null);
                    Phone.setText(null);
                    Email.setText(null);
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, "Usuario cadastrado com sucesso!!", Toast.LENGTH_LONG);
                    toast.show();
                }
                else{
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, "Falha ao cadastrar!", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        btnMail = (Button) findViewById(R.id.btnMainActivityMail);
        btnMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                EditText Name = (EditText) findViewById(R.id.editTextMainActivityName);
                EditText Phone = (EditText) findViewById(R.id.editTextMainActivityPhoneNumber);
                EditText Email = (EditText) findViewById(R.id.editTextMainActivityEmail);

                String Mail = "";
                if(Name.getText() != null)
                    Mail += "Nome: " + Name.getText();
                if(Phone.getText() != null)
                    Mail += " // Telefone: " + Phone.getText();
                if(Email.getText() != null)
                    Mail += " // Email: " + Email.getText();
                if(cb1.isChecked())
                    Mail += " // Sexo: Masculino";
                if(cb2.isChecked())
                    Mail += " // Sexo: Feminino";

                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setType("text/plain");
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL, Email.getText());
                intent.putExtra(Intent.EXTRA_SUBJECT, "Abrindo APP com dados.");
                intent.putExtra(Intent.EXTRA_TEXT, Mail);
                if(HasImage){
                    File f = new File(currentPhotoPath);
                    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
                }
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });

        btnImage = (ImageButton) findViewById(R.id.btnCaptureImage);
        btnImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Context context = getApplicationContext();
                try{
                    uriSaveImage = FileProvider.getUriForFile(context, "com.example.trab1_joao.fileprovider", createImageFile());
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSaveImage);

                    if(intent.resolveActivity(getPackageManager()) != null){
                        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            private File createImageFile() throws IOException {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp + "_";
                File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                File image = File.createTempFile(
                        imageFileName,
                        ".jpg",
                        storageDir
                );

                currentPhotoPath = image.getAbsolutePath();
                return image;
            }
        });

        btnUserList = (Button) findViewById(R.id.btnMainActivityListView);
        btnUserList.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Context context = getApplicationContext();
                Intent intent = new Intent(context, UserListActivity.class);
                startActivity(intent);
            }
        });

        /*btnConfiguration = (Button) findViewById(R.id.btnMainActivityConfig);
        btnConfiguration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Context context = getApplicationContext();
                Intent intent = new Intent(context, Configuration.class);
                startActivity(intent);
            }
        });

        btnLayoutTop = (Button) findViewById(R.id.btnMainActivityLayout);
        btnLayoutTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Context context = getApplicationContext();
                Intent intent = new Intent(context, LayoutTop.class);
                startActivity(intent);
            }
        });

        btnDataView = (Button) findViewById(R.id.btnMainActivityView);
        btnDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Context context = getApplicationContext();
                EditText Name = (EditText) findViewById(R.id.editTextMainActivityName);
                EditText Phone = (EditText) findViewById(R.id.editTextMainActivityPhoneNumber);
                EditText Email = (EditText) findViewById(R.id.editTextMainActivityEmail);

                Intent intent = new Intent(context, DataView.class);
                intent.putExtra("sName", Name.getText());
                intent.putExtra("sPhone", Phone.getText());
                intent.putExtra("sEmail", Email.getText());
                intent.putExtra("cb1", cb1.isChecked());
                intent.putExtra("cb2", cb2.isChecked());
                startActivity(intent);
            }
        });*/

        cb1.setChecked(true);
        cb1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                cb1.setChecked(true);
                cb2.setChecked(false);
            }
        });
        cb2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                cb1.setChecked(false);
                cb2.setChecked(true);
            }
        });
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            TextView text = (TextView) findViewById(R.id.txtImageNotFound);
            text.setText("Imagem anexada!");
            HasImage = true;
        }
    }
}