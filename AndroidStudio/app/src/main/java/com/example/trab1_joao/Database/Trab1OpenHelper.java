package com.example.trab1_joao.Database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Trab1OpenHelper extends SQLiteOpenHelper {
    public static Trab1OpenHelper INSTANCIA_CONEXAO;
    public static final String DATABASE_NAME = "Trab1.db";
    public static final int DATABASE_VERSION = 1;
    public Trab1OpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static Trab1OpenHelper getInstancia(Context context){
        if(INSTANCIA_CONEXAO == null){
            INSTANCIA_CONEXAO = new Trab1OpenHelper(context);
        }
        return INSTANCIA_CONEXAO;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Trab1DatabaseContract.UserInfoEntry.SQL_CREATE_TABLE);
        db.execSQL(Trab1DatabaseContract.UserInfoEntry.SQL_CREATE_INDEX1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < 2) {
            //Novas tabelas de novas versões do app aqui
        }
    }
}
